const http = require('http');
const https = require('https');

// Get the pod's IP, name, namespace
const hostname = process.env.HOSTNAME || 'localhost';
const podName = process.env.POD_NAME || 'unknown';
const podNamespace = process.env.POD_NAMESPACE || 'default';

// Create an HTTP server
const server = http.createServer((req, res) => {
  // Set response status code and headers
  res.writeHead(200, {'Content-Type': 'text/html'});

  // Echo the incoming request body and pod info back to the client
  let responseBody = `
    <html>
      <head>
        <title>Pod Information</title>
      </head>
      <body>
        <h1>Pod information for pod ${podName} (${hostname}) in namespace ${podNamespace}</h1>
      </body>
    </html>  
  `;
  req.on('data', (chunk) => {
      responseBody += chunk;
  });
  req.on('end', () => {
      res.end(responseBody);
  });
});

// Start listening for incoming requests
const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
 console.log(`Server running on port ${PORT}`);
});